type Adjustment
    close::Float64
    adjustmenttype::String
    adjustmentfactor::Float64
end
