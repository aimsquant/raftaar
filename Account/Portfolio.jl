# © AIMSQUANT PVT. LTD.
# Author: Shiv Chawla
# Email: shiv.chawla@aimsquant.com
# Organization: AIMSQUANT PVT. LTD.

"""
Type to encapsulate the aggregated analytics like 
various exposures and security counts
"""
type PortfolioMetrics
  netexposure::Float64
  grossexposure::Float64
  shortexposure::Float64
  longexposure::Float64
  shortcount::Float64
  longcount::Float64
end

PortfolioMetrics() = PortfolioMetrics(0.0, 0.0, 0.0, 0.0, 0, 0)

"""
Type to encapsulate positions and aggregated metrics
"""
type Portfolio
  positions::Dict{SecuritySymbol, Position}
  metrics::PortfolioMetrics
end

Portfolio() = Portfolio(Dict(), PortfolioMetrics())

"""
Indexing function to get position based 
on security symbol or security directly from portfolio
"""

getindex(portfolio::Portfolio, symbol::SecuritySymbol) = get(portfolio.positions, symbol, Position(symbol))
getindex(portfolio::Portfolio, security::Security) = get(portfolio.positions, security.symbol, Position(security.symbol))
setindex!(portfolio::Portfolio, position::Position, securitysymbol::SecuritySymbol) = 
                      setindex!(portfolio.positions, position, securitysymbol)

"""
function to get all positions in a portfolio
"""
function getallpositions(portfolio::Portfolio)
  values(portfolio.positions)
end


function getposition(portfolio::Portfolio, ss::SecuritySymbol)
  return portfolio[ss]
end
export getposition

"""
function to update/set position with average price and quantity
"""
function setposition!(portfolio::Portfolio, security::Security, avgprice::Float64, quantity::Int64)
  position = getposition(portfolio, security)

  if empty(position)
    return
  else
    position.quantity = quantity
    position.averageprice = avgprice  
  end
end

"""
function to update/set position with average price and quantity
"""
function setposition!(portfolio::Portfolio, symbol::SecuritySymbol, avgprice::Float64, quantity::Int64)
  position = getposition(portfolio, symbol)

  if empty(position)
    return
  else
    position.quantity = quantity
    position.averageprice = avgprice  
  end
end

#="""
function to get portfolio value
"""
function getportfoliovalue(portfolio::Portfolio)
  pv = 0
  for (sec, pos) in enumerate(portfolio.positions)
    pv += holdingvalue(pos)
  end
  return pv
end

"""
function to get netexposure of the portfolio
"""
function getnetexposure(portfolio::Portfolio)
  portfolio.metrics.netexposure
end

"""
function to get gross exposure
"""
function getgrossexposure(portfolio::Portfolio)
  portfolio.metrics.grossexposure
end=#

"""
function to get absolute of holding cost
"""
function totalabsoluteholdingscost(portfolio::Portfolio)
  tahc = 0
  for (sec, pos) in enumerate(portfolio.positions)
    tahc += absholdingcost(pos)
  end
  return tahc
end

"""
function to update portfolio with multiple fills
"""
function updateportfolio_fills!(portfolio::Portfolio, fills::Vector{OrderFill})
  cash = 0.0 
  for fill in fills 
    cash += updateportfolio_fill!(portfolio, fill)
  end

  updateportfoliometrics!(portfolio::Portfolio)

  return cash
end

"""
function to update portfolio for single fill
"""
function updateportfolio_fill!(portfolio::Portfolio, fill::OrderFill)
   
  securitysymbol = fill.securitysymbol  
  
  if !haskey(portfolio.positions, securitysymbol)
      portfolio[securitysymbol] = Position(securitysymbol)  
  end  

  position = portfolio[securitysymbol]
  
  #function to adjust position for fill and update cash in portfolio
  return updateposition_fill!(position, fill)

end

"""
function to update portfolio for a split
"""  
function updateportfolioforsplit!(portfolio::Portfolio, split::Split)
  position = portfolio[split.symbol]
   
  if !empty(position)
      quantity = position.quantity/split.splitFactor
      avgprice = position.averagePrice*split.splitFactor

      #we'll model this as a cash adjustment
      leftOver = quantity - (Int64)quantity
      extraCash = leftOver*split.ReferencePrice
      addcash!(portfolio, extraCash);
      setposition!(portfolio, split.symbol, avgprice, (int)quantity)
    end       
end 

"""
function to update portfolio for a split
"""
function updateportfolio_price!(portfolio::Portfolio, tradebars::Dict{SecuritySymbol, Vector{TradeBar}}, datetime::DateTime)

  for position in getallpositions(portfolio)
      securitysymbol = position.securitysymbol
      if haskey(tradebars, securitysymbol)
        updateposition_price!(position, tradebars[securitysymbol][1])
      end
  end 

  updateportfoliometrics!(portfolio::Portfolio)
end

function updateportfolio_splits_dividends!(portfolio::Portfolio, adjustments::Dict{SecuritySymbol, Adjustment})
    for (symbol, adjustment) in adjustments
        
        if (adjustment.adjustmenttype != "17.0" && portfolio[symbol].quantity != 0)
            updateposition_splits_dividends!(portfolio[symbol], adjustment)
        end

    end 

    updateportfoliometrics!(portfolio::Portfolio) 
end

"""
function to update portfolio metrics
"""
function updateportfoliometrics!(portfolio::Portfolio) 
  
  portfolio.metrics = PortfolioMetrics()

  for (symbol, position) in portfolio.positions
    portfolio.metrics.netexposure += position.quantity * position.lastprice  
    portfolio.metrics.grossexposure += abs(position.quantity * position.lastprice)
    portfolio.metrics.shortexposure += position.quantity < 0 ? abs(position.quantity * position.lastprice) : 0.0
    portfolio.metrics.shortcount +=  position.quantity < 0 ? 1 : 0
    portfolio.metrics.longexposure += position.quantity > 0 ? abs(position.quantity * position.lastprice) : 0.0
    portfolio.metrics.longcount += position.quantity > 0 ? 1 : 0
  end

end



